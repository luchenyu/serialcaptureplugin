﻿using ReadParseTGAM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SerialCaptureManager : MonoBehaviour
{
 
    public static SerialCaptureManager instance;
    //public string Port = "COM3";
    //public int BaudRate = 9600;
    //public int DataBits = 8;
    public List<string> portlist;
    public Dictionary<string, string> dataList;
    COMPortManager manager;

    public delegate void DeviceReceiveDelegate(List<string> data);
    public event DeviceReceiveDelegate DeviceReceiveEvent;//设备更新事件
    public delegate void DataReceiveDelegate(Dictionary<string, string> dataList);
    public event DataReceiveDelegate DataReceiveEvent;//数据接收事件
    public delegate void ConnectedDelegate();
    public event ConnectedDelegate ConnectedEvent;//连接成功事件
    private void Awake()
    {
        dataList = new Dictionary<string, string>();
        instance = this;
        Parser parser = new Parser();
        manager = new COMPortManager(parser);
    }
    private void OnDisable()
    {
        manager.DisConnect();
    }

    //扫描串口
    public void StartScan()
    {
        var list = manager.GetCOMList();
        portlist = new List<string>(list);
        DeviceReceiveEvent?.Invoke(portlist);

    }
    //连接串口
    public void Connect(int index)
    {
        manager.Connect(portlist[index]);
        ConnectedEvent?.Invoke();
    }
    public void UpdateData(string name, string value)
    {
        if (dataList.ContainsKey(name))
        {
            dataList[name] = value;
        }
        else
        {
            dataList.Add(name, value);
        }
        DataReceiveEvent?.Invoke(dataList);
    }



    

}

