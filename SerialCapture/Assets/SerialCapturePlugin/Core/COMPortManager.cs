﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;
using System.Threading;
using UnityEngine;

namespace ReadParseTGAM
{
    class COMPortManager
    {
        Parser parser;
        SerialPort chris_COM;
        Thread readThread;
        bool keepReading;

        public string[] GetCOMList()
        {
            return SerialPort.GetPortNames();

        }
        public void Connect(string portName)
        {
            chris_COM = new SerialPort(portName);
            //chris_COM.BaudRate = SerialCaptureManager.instance.BaudRate;
            //chris_COM.PortName = portName;
            //chris_COM.DataBits = SerialCaptureManager.instance.DataBits;
            chris_COM.Open();

            keepReading = true;
            readThread = new Thread(ReadPort);
            readThread.Start();
        }
        public void DisConnect() {
            chris_COM.Close();
        }
        public COMPortManager(Parser p)
        {
            parser = p;


            //chris_COM.DataReceived += new SerialDataReceivedEventHandler(OnDataReceived);
        }

        private void OnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            Debug.Log(sender);


        }

        //读取数据
        private void ReadPort()
        {

            while (keepReading)
            {
                if (chris_COM.IsOpen)
                {

                    byte[] chris_buffer = new byte[chris_COM.ReadBufferSize + 1];

                    int count = chris_COM.Read(chris_buffer, 0, chris_COM.ReadBufferSize);


                    for (int i = 0; i < count; i++)
                    {
                        parser.parseByte(chris_buffer[i]);
                        //Console.WriteLine("XX:" + i);
                    }

                    //Console.WriteLine(count);


                }




            }


        }









    }
}
