﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class demo : MonoBehaviour {
    public GameObject devicePrefab;
    public Transform content;
    public Text 提示;
    public Text 数据;
    public GameObject dataPanel;
    string data;
    bool dataChange = false;
    // Use this for initialization
    void Start () {

        SerialCaptureManager.instance.DeviceReceiveEvent += UpdateDeviceList;
        SerialCaptureManager.instance.DataReceiveEvent += UpdateDataList;
        SerialCaptureManager.instance.ConnectedEvent += ShowData;

        SerialCaptureManager.instance.StartScan();
        提示.text = "扫描中...";
    }
    private void Update()
    {
        if (dataChange) {
            数据.text = data;
            dataChange = false;
        }
        
    }
    public void ShowData() {
        提示.text = "连接成功";
        GameObject.Destroy(提示.gameObject, 1f);
        dataPanel.SetActive(true);
    }
    public void GenerateListUnit(int i, string info)
    {
        //添加UI
        content.GetComponent<RectTransform>().sizeDelta = new Vector2(0, (i + 1) * 110);

        //实例化一个列表子元素
        GameObject o = Instantiate(devicePrefab);
        o.transform.SetParent(content);
        o.SetActive(true);
        //修改信息
        Transform trans = o.transform;
        Text infoText = trans.Find("infoText").GetComponent<Text>();
        infoText.text = info;
        //按钮事件
        Button btn = trans.Find("connectButton").GetComponent<Button>();
        btn.name = i.ToString();   //改变按钮的名字，以便传参
        btn.onClick.AddListener(delegate ()
        {
            int index = int.Parse(btn.name);
            提示.text = "连接"+SerialCaptureManager.instance.portlist[index];
            SerialCaptureManager.instance.Connect(index);//连接设备
        }
        );
    }

    //更新UI列表
    public void UpdateDeviceList(List<string> deviceList)
    {
        /**清空UI列表*/
        for (int i = 0; i < content.childCount; i++)
        {
            DestroyImmediate(content.GetChild(i).gameObject);
        }
        /**生成UI列表*/
        for (int i = 0; i < deviceList.Count; i++)
        {
            GenerateListUnit(i, deviceList[i]);
        }
    }

    //更新数据列表的UI
    public void UpdateDataList(Dictionary<string, string> datalist)
    {
        data = "";
        foreach (var a in datalist)
        {
            data += (a.Key + ":" + a.Value + "\n");
        }
        dataChange = true;
    }
}
